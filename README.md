# Java 8 Ubuntu container

This is a base container for all [Java 8][2] [Ubuntu][1] containers. 


## To Build

```
> docker build --tag codered/ubuntu-java8 . # normal build
> docker build --no-cache=true --force-rm=true --tag codered/ubuntu-java8. # force a full build
> docker push codered/ubuntu-java8 # send it to docker hub
```

## To Run

```
>  docker run -t -i codered/ubuntu-java8


```

[1]:  http://www.ubuntu.com/
[2]:  http://www.oracle.com/technetwork/java/javase/overview/index.html

